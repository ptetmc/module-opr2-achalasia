<?php

function opr2_achalasia_bundle_register_achalasia() {
  $bundle = array(
    'machine_name' => 'register_achalasia',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => array(array('value'=>'v1')),
      ),
    ), // end instances

    'groups' => array(
      'group_epic' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'A hospitalizáció rövid összefoglalója, beleértve, hogy hogyan került a beteg a kórházba, klinikára, mi történt vele a bentfekvés alatt és milyen javaslattal és hová távozott (kontroll vizsgálat, műtét, stb.)',
            'notes_en' => 'A short summary of the  hospitalization (how the patient got to medical care, diagnosis, most important facts and events of the hospitalization, what happened with the patient after the hospitalization, any recommended control examinations, surgery).',
          ),
        ),
      ),
    ), // end groups

    'tree' => array(
      'field_form_version',

      'field_inpatient',

      'group_inline' => array(
        'group_dg_when' => array(
          'field_dg_when',
          'field_dg_date',
        ),

        'group_dg' => array(
          'group_dg_ogb' => array(
            'field_dg_ogb_date',
            'field_dg_ogb_mech',
            'field_dg_ogb_myc',
            'field_dg_ogb_slump',
            'group_dg_ogb_gast' => array(
              'field_dg_ogb_gast',
              'field_dg_ogb_gast_type',
            ),
            'field_dg_ogb_hrpos',
          ),
          'group_dg_swalrad' => array(
            'field_dg_swalrad_date',
            'field_dg_swalrad_eso',
            'field_dg_swalrad_ret',
            'field_dg_swalrad_card',
          ),
          'group_dg_esoman' => array(
            'field_dg_esoman_type',
            'field_dg_esoman_date',
            'field_dg_esoman_lespos',
            'field_dg_esoman_lesbp',
            'field_dg_esoman_lesrel',
            'field_dg_esoman_tubmot',
            'field_dg_esoman_uespos',
            'field_dg_esoman_uesbp',
            'field_dg_esoman_uesrel',
            'field_dg_esoman_chic',
          ),
        ), // end group_dg

        'group_anamn' => array(
          'group_alcohol' => array(
            'field_alcohol_consumption',
            'field_alcohol_freq',
            'field_alcohol_amount2',
            'field_alcohol_since2',
            'field_alcohol_2w_amount',
            'group_alcohol_past' => array(
              'field_alcohol_past',
              'field_alcohol_past_freq',
              'field_alcohol_past_amount',
              'field_alcohol_past_years',
              'field_alcohol_past_ended2',
            ),
          ),
          'group_smoking' => array(
            'field_smoking',
            'field_smoking_amount2',
            'field_smoking_since2',
            'group_smoking_past' => array(
              'field_smoking_past',
              'field_smoking_past_amount',
              'field_smoking_past_years',
              'field_smoking_past_ended2',
            ),
          ),
          'group_drugs' => array(
            'field_drug_consumption',
            'field_drug_name',
            'field_drug_amount',
            'field_drug_since3',
          ),
          'field_famtum',
          'group_otherdis' => array(
            'field_otherdis',
            'field_otherdis_type',
          ),
          'group_medi' => array(
            'field_medi',
            'field_medi_detail2',
          ),
          'group_diet' => array(
            'field_diet',
            'field_diet_type',
          ),
          'field_history',
        ), // group_anamn

        'group_orsym' => array(
          'group_orsym_swal' => array(
            'field_orsym_swal',
            'field_orsym_swal_since',
            'field_orsym_swal_food',
            'field_orsym_swal_freq',
          ),
          'group_orsym_chpr' => array(
            'field_orsym_chpr',
            'field_orsym_chpr_freq',
          ),
          'group_orsym_regur' => array(
            'field_orsym_regur',
            'field_orsym_regur_freq',
          ),
          'group_orsym_vomit' => array(
            'field_orsym_vomit',
            'field_orsym_vomit_freq',
          ),
          'field_orsym_appet',
          'group_orsym_weight' => array(
            'field_orsym_weight',
            'field_orsym_weight_sgn',
            'field_orsym_weight_time',
            'field_orsym_weight_amount',
          ),
          'field_orsym_cough',

          'group_orsym_ther' => array(
            'group_orsym_thmed' => array(
              'field_orsym_thmed',
              'field_orsym_thmed_swal',
              'field_orsym_thmed_chpr',
              'field_orsym_thmed_regur',
              'field_orsym_thmed_vomit',
              'field_orsym_thmed_appet',
              'field_orsym_thmed_weight',
              'field_orsym_thmed_cough',
              'field_orsym_thmed_esoburn',
              'field_orsym_thmed_acid',
              'field_orsym_thmed_epiburn',
            ),
            'group_orsym_thbotox' => array(
              'field_orsym_thbotox',
              'field_orsym_thbotox_swal',
              'field_orsym_thbotox_chpr',
              'field_orsym_thbotox_regur',
              'field_orsym_thbotox_vomit',
              'field_orsym_thbotox_appet',
              'field_orsym_thbotox_weight',
              'field_orsym_thbotox_cough',
              'field_orsym_thbotox_esoburn',
              'field_orsym_thbotox_acid',
              'field_orsym_thbotox_epiburn',
            ),
            'group_orsym_thbal' => array(
              'field_orsym_thbal',
              'field_orsym_thbal_swal',
              'field_orsym_thbal_chpr',
              'field_orsym_thbal_regur',
              'field_orsym_thbal_vomit',
              'field_orsym_thbal_appet',
              'field_orsym_thbal_weight',
              'field_orsym_thbal_cough',
              'field_orsym_thbal_esoburn',
              'field_orsym_thbal_acid',
              'field_orsym_thbal_epiburn',
            ),
            'group_orsym_thhmy' => array(
              'field_orsym_thhmy',
              'field_orsym_thhmy_swal',
              'field_orsym_thhmy_chpr',
              'field_orsym_thhmy_regur',
              'field_orsym_thhmy_vomit',
              'field_orsym_thhmy_appet',
              'field_orsym_thhmy_weight',
              'field_orsym_thhmy_cough',
              'field_orsym_thhmy_esoburn',
              'field_orsym_thhmy_acid',
              'field_orsym_thhmy_epiburn',
            ),
          ),
          'group_orsym_swalrad' => array(
            'field_orsym_swalrad',
            'field_orsym_swalrad_date',
            'field_orsym_swalrad_eso',
            'field_orsym_swalrad_ret',
            'field_orsym_swalrad_card',
          ),
          'group_orsym_esoman' => array(
            'field_orsym_esoman_lesbp',
            'field_orsym_esoman_lesrel',
          ),
        ), // end group_orsym

        'group_sym' => array(
          'field_sym',
          'group_sym_swal' => array(
            'field_sym_swal',
            'field_sym_swal_since',
            'field_sym_swal_food',
            'field_sym_swal_freq',
          ),
          'group_sym_chpr' => array(
            'field_sym_chpr',
            'field_sym_chpr_freq',
          ),
          'group_sym_regur' => array(
            'field_sym_regur',
            'field_sym_regur_freq',
          ),
          'group_sym_vomit' => array(
            'field_sym_vomit',
            'field_sym_vomit_freq',
          ),
          'field_sym_appet',
          'group_sym_weight' => array(
            'field_sym_weight',
            'field_sym_weight_sgn',
            'field_sym_weight_time',
            'field_sym_weight_amount',
          ),
          'field_sym_cough',
          'field_sym_eckard',
        ), // end group_sym

        'group_status' => array(
          'field_weight',
          'field_height',
          'field_bmi',
          'field_abdomtender',
        ),

        'group_ther' => array(
          'group_ther_med' => array(
            'field_ther_med',
            'field_ther_med_type',
            'field_ther_med_dose',
            'field_ther_med_times',
          ),
          'group_ther_botox' => array(
            'field_ther_botox',
            'field_ther_botox_dose',
            'field_ther_botox_times',
          ),
          'group_ther_baldil' => array(
            'field_ther_baldil',
            'field_ther_baldil_type',
            'field_ther_baldil_len',
            'field_ther_baldil_dil',
            'group_ther_baldil_rep' => array(
              'field_ther_baldil_rep',
              'field_ther_baldil_rep_times',
            ),
            'group_ther_baldil_comp' => array(
              'field_ther_baldil_comp',
              'field_ther_baldil_comp_type',
            ),
          ),
          'group_ther_poem' => array(
            'field_ther_poem',
            'group_ther_poem_comp' => array(
              'field_ther_poem_comp',
              'field_ther_poem_comp_type',
            ),
            'field_ther_poem_rep',
          ),
          'group_ther_hemy' => array(
            'field_ther_hemy',
            'field_ther_hemy_type',
            'group_ther_hemy_comp' => array(
              'field_ther_hemy_comp',
              'field_ther_hemy_comp_type',
            ),
            'field_ther_hemy_reop',
          ),
          'group_ther_swalrad' => array(
            'field_ther_swalrad_eso',
            'field_ther_swalrad_ret',
            'field_ther_swalrad_card',
          ),
        ), // end group_ther

        'group_epic' => array(
          'field_epic',
        ),
      ),
    ), // end tree
  );

  return $bundle;
}
