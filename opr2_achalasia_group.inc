<?php

function opr2_achalasia_group($name, $bundle) {
  switch ($name) {
  case 'group_inline':
    return _opr2grp('inline');
  case 'group_dg_when':
    return _opr2grp('category', '2. A diagnózis felállításának ideje');
  case 'group_dg':
    return _opr2grp('category', '3. A diagnózis alapja');
  case 'group_dg_ogb':
    return _opr2grp('div', 'Oesophago-gastro-bulboscopia');
  case 'group_dg_ogb_gast':
    return _opr2grp('conditional', '');
  case 'group_dg_swalrad':
    return _opr2grp('div', 'Nyelési RTG');
  case 'group_dg_esoman':
    return _opr2grp('div', 'Nyelőcső manometria');
  case 'group_anamn':
    return _opr2grp('category', '4. Anamnesztikus adatok');
  case 'group_alcohol':
    return _opr2grp('conditional', '', array(
      'col_map' => "3\n2\n4\n6\n4",
    ));
  case 'group_alcohol_past':
    return _opr2grp('conditional', '');
  case 'group_smoking':
    return _opr2grp('conditional', '', array(
      'col_map' => "3\n6\n4",
    ));
  case 'group_smoking_past':
  case 'group_drugs':
  case 'group_otherdis':
  case 'group_medi':
  case 'group_diet':
    return _opr2grp('conditional', '');

  case 'group_orsym':
    return _opr2grp('category', '5. Eredeti panaszok');
  case 'group_orsym_swal':
  case 'group_orsym_chpr':
  case 'group_orsym_regur':
  case 'group_orsym_vomit':
  case 'group_orsym_weight':
  case 'group_sym_swal':
  case 'group_sym_chpr':
  case 'group_sym_regur':
  case 'group_sym_vomit':
  case 'group_sym_weight':
    return _opr2grp('conditional', '');
  case 'group_orsym_ther':
    return _opr2grp('div', 'Korábbi beavatkozások');
  case 'group_orsym_thmed':
  case 'group_orsym_thbotox':
  case 'group_orsym_thbal':
  case 'group_orsym_thhmy':
  case 'group_orsym_swalrad':
  case 'group_orsym_esoman':
    return _opr2grp('conditional', '');

  case 'group_sym':
    return _opr2grp('category', '6. Jelen panaszok, tünetek');

  case 'group_status':
    return _opr2grp('category', '7. Felvételi adatok, status');

  case 'group_ther':
    return _opr2grp('category', '8. Terápia');
  case 'group_ther_med':
  case 'group_ther_botox':
  case 'group_ther_baldil':
  case 'group_ther_baldil_rep':
  case 'group_ther_baldil_comp':
  case 'group_ther_poem':
  case 'group_ther_poem_comp':
  case 'group_ther_hemy':
  case 'group_ther_hemy_comp':
  case 'group_ther_swalrad':
    return _opr2grp('conditional', '');

  case 'group_epic':
    return _opr2grp('category', '9. Epikrízis');
  }
}
