<?php

function opr2_achalasia_field($name) {
  switch ($name) {
  case 'field_dg_when':
    return _opr2fld('number_list', 'Új diagnózis', array(
      'options' => array(
        0 => 'korábban diagnosztizált',
        1 => 'újonnan diagnosztizált',
      ),
    ));
  case 'field_dg_date':
    return _opr2fld('date_partial_day', 'A diagnózis időpontja');
  case 'field_dg_ogb_date':
  case 'field_dg_swalrad_date':
  case 'field_dg_esoman_date':
  case 'field_orsym_swalrad_date':
    return _opr2fld('date_partial_day', 'vizsgálat dátuma', array(
      'required' => TRUE,
    ));
  case 'field_dg_ogb_mech':
    return _opr2fld('boolean_radio', 'mechanikus obstrukció jelenléte', array(
      'required' => TRUE,
    ));
  case 'field_dg_ogb_myc':
    return _opr2fld('boolean_radio', 'nyelőcső mycosis jelenléte');
  case 'field_dg_ogb_slump':
    return _opr2fld('boolean_radio', 'ételpangás a nyelőcsőben');
  case 'field_dg_ogb_gast':
    return _opr2fld('boolean_radio', 'gyomornyálkahártya eltérés');
  case 'field_dg_ogb_gast_type':
    return _opr2fld('select_or_other', 'leírása', array(
      'options' => array(
        1 => 'gyulladás',
        2 => 'erosio',
        3 => 'fekély',
        4 => 'polyp',
        5 => 'tumor',
      ),
    ));
  case 'field_dg_ogb_hrpos':
    return _opr2fld('boolean_radio', 'HR pozitivitás');
  case 'field_dg_swalrad_eso':
  case 'field_orsym_swalrad_eso':
  case 'field_ther_swalrad_eso':
    return _opr2fld('number_decimal', 'nyelőcső tágassága', array(
      'suffix' => 'cm',
    ));
  case 'field_dg_swalrad_ret':
  case 'field_orsym_swalrad_ret':
  case 'field_ther_swalrad_ret':
    return _opr2fld('number_decimal', 'retenció mértéke', array(
      'suffix' => 'cm',
    ));
  case 'field_dg_swalrad_card':
  case 'field_orsym_swalrad_card':
  case 'field_ther_swalrad_card':
    return _opr2fld('number_decimal', 'cardia tágassága', array(
      'suffix' => 'mm',
    ));
  case 'field_dg_esoman_type':
    return _opr2fld('number_list', 'A manometria típusa', array(
      'options' => array(
        1 => 'folyadékperfúziós',
        2 => 'high res folyadékperf',
        3 => 'high res solid state',
      ),
    ));
  case 'field_dg_esoman_lespos':
    return _opr2fld('range_decimal', 'LES helyzete', array(
      'suffix' => 'cm',
    ));
  case 'field_dg_esoman_lesbp':
    return _opr2fld('number_decimal', 'LES alapnyomás', array(
      'suffix' => 'Hgmm',
    ));
  case 'field_dg_esoman_lesrel':
    return _opr2fld('number_integer', 'LES relaxatio', array(
      'suffix' => '%',
    ));
  case 'field_dg_esoman_tubmot':
    return _opr2fld('select_or_other', 'Tubularis nyelőcső motilitas', array(
      'options' => array(
        1 => 'aperisztaltika',
        2 => 'szimultan 30 Hgmm alatti kontrakciók',
        3 => 'szimultan 30 Hgmm feletti kontrakciók',
        4 => 'inkoordinált mozgás',
      ),
    ));
  case 'field_dg_esoman_uespos':
    return _opr2fld('range_decimal', 'UES helyzete', array(
      'suffix' => 'cm',
    ));
  case 'field_dg_esoman_uesbp':
    return _opr2fld('number_decimal', 'UES alapnyomás', array(
      'suffix' => 'Hgmm',
    ));
  case 'field_dg_esoman_uesrel':
    return _opr2fld('number_integer', 'UES relaxatio', array(
      'suffix' => '%',
    ));
  case 'field_dg_esoman_chic':
    return _opr2fld('number_list', 'Chicago classificatio', array(
      'options' => array(
        1 => 'I. klasszikus',
        2 => 'II. klasszikus achalasia nyelőcső kompressziókkal',
        3 => 'III. spasztikus',
      ),
    ));
  case 'field_alcohol_consumption':
    return _opr2fld('boolean_radio', 'Alkoholfogyasztás');
  case 'field_alcohol_freq':
  case 'field_alcohol_past_freq':
    return _opr2fld('number_list', 'gyakoriság', array(
      'options' => array(
        1 => 'alkalmanként',
        2 => 'havonta',
        3 => 'hetente',
        4 => 'naponta',
      ),
    ));
  case 'field_alcohol_amount2':
  case 'field_alcohol_past_amount':
    return _opr2fld('number_list', 'mennyiség alkalmanként', array(
      'options' => array(
        25 => '0-25g',
        50 => '26-50g',
        75 => '51-75g',
        100 => '76-100g',
        125 => '101-125g',
        150 => '126-150g',
        175 => '151-175g',
        200 => '176-200g',
        225 => '201-225g',
        250 => '226-250g',
        251 => 'több, mint 250g',
      ),
    ));
  case 'field_alcohol_since2':
  case 'field_smoking_since2':
  case 'field_drug_since3':
    return _opr2fld('number_list', 'hány éve', array(
      'options' => array(
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16',
        17 => '17',
        18 => '18',
        19 => '19',
        20 => '20',
        21 => '21',
        22 => '22',
        23 => '23',
        24 => '24',
        25 => '25',
        26 => '26',
        27 => '27',
        28 => '28',
        29 => '29',
        30 => '30',
        31 => '31',
        32 => '32',
        33 => '33',
        34 => '34',
        35 => '35',
        36 => '36',
        37 => '37',
        38 => '38',
        39 => '39',
        40 => '40',
        41 => '41',
        42 => '42',
        43 => '43',
        44 => '44',
        45 => '45',
        46 => '46',
        47 => '47',
        48 => '48',
        49 => '49',
        50 => '50',
        51 => 'több, mint 50',
      ),
    ));
  case 'field_alcohol_2w_amount':
    return _opr2fld('number_list', 'Az elmúlt két hétben fogyasztott alkohol összes mennyisége', array(
      'options' => array(
        25 => '0-25g',
        50 => '26-50g',
        75 => '51-75g',
        100 => '76-100g',
        125 => '101-125g',
        150 => '126-150g',
        175 => '151-175g',
        200 => '176-200g',
        225 => '201-225g',
        250 => '226-250g',
        251 => 'több, mint 250g',
      ),
    ));
  case 'field_alcohol_past':
    return _opr2fld('boolean_radio', 'Korábban fogyasztott-e alkoholt');
  case 'field_alcohol_past_years':
  case 'field_smoking_past_years':
    return _opr2fld('number_list', 'Hány évet', array(
      'options' => array(
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => '10',
        11 => '11',
        12 => '12',
        13 => '13',
        14 => '14',
        15 => '15',
        16 => '16',
        17 => '17',
        18 => '18',
        19 => '19',
        20 => '20',
        21 => '21',
        22 => '22',
        23 => '23',
        24 => '24',
        25 => '25',
        26 => '26',
        27 => '27',
        28 => '28',
        29 => '29',
        30 => '30',
        31 => '31',
        32 => '32',
        33 => '33',
        34 => '34',
        35 => '35',
        36 => '36',
        37 => '37',
        38 => '38',
        39 => '39',
        40 => '40',
        41 => '41',
        42 => '42',
        43 => '43',
        44 => '44',
        45 => '45',
        46 => '46',
        47 => '47',
        48 => '48',
        49 => '49',
        50 => '50',
        51 => 'több, mint 50',
      ),
    ));
  case 'field_alcohol_past_ended2':
    return _opr2fld('number_list', 'Mennyi ideje hagyott fel az alkoholfogyasztással', array(
      'options' => array(
        1 => "1 hét",
        2 => "2 hét",
        3 => "3 hét",
        4 => "4 hét",
        5 => "5 hét",
        6 => "6 hét",
        7 => "7 hét",
        8 => "8 hét",
        12 => "3 hónap",
        16 => "4 hónap",
        20 => "5 hónap",
        24 => "6 hónap",
        28 => "7 hónap",
        32 => "8 hónap",
        36 => "9 hónap",
        40 => "10 hónap",
        44 => "11 hónap",
        52 => "1 év",
        78 => "1,5 év",
        104 => "2 év",
        156 => "3 év",
        208 => "4 év",
        260 => "5 év",
        261 => "több, mint 5 év",
      ),
    ));
  case 'field_smoking':
    return _opr2fld('boolean_radio', 'Dohányzás');
  case 'field_smoking_amount2':
  case 'field_smoking_past_amount':
    return _opr2fld('number_list', 'mennyiség (cigaretta/nap)', array(
      'options' => array(
        5 => 'kevesebb, mint 5',
        10 => '5-10',
        15 => '11-15',
        20 => '16-20',
        25 => '21-25',
        30 => '26-30',
        35 => '31-35',
        40 => '36-40',
        41 => 'több, mint 40',
      ),
    ));
  case 'field_smoking_past':
    return _opr2fld('boolean_radio', 'Korábban dohányzott-e');
  case 'field_smoking_past_ended2':
    return _opr2fld('number_list', 'Mennyi ideje hagyta abba a dohányzást', array(
      'options' => array(
        1 => "1 hét",
        2 => "2 hét",
        3 => "3 hét",
        4 => "4 hét",
        5 => "5 hét",
        6 => "6 hét",
        7 => "7 hét",
        8 => "8 hét",
        12 => "3 hónap",
        16 => "4 hónap",
        20 => "5 hónap",
        24 => "6 hónap",
        28 => "7 hónap",
        32 => "8 hónap",
        36 => "9 hónap",
        40 => "10 hónap",
        44 => "11 hónap",
        52 => "1 év",
        78 => "1,5 év",
        104 => "2 év",
        156 => "3 év",
        208 => "4 év",
        260 => "5 év",
        261 => "több, mint 5 év",
      ),
    ));
  case 'field_drug_consumption':
    return _opr2fld('boolean_radio', 'Drogfogyasztás');
  case 'field_drug_name':
    return _opr2fld('text', 'drog megnevezése');
  case 'field_drug_amount':
    return _opr2fld('text', 'mennyisége');
  case 'field_famtum':
    return _opr2fld('boolean_radio', 'Családban előforduló daganatos betegség');
  case 'field_otherdis':
    return _opr2fld('boolean_radio', 'Egyéb betegségek');
  case 'field_otherdis_type':
    return _opr2fld('text', 'megnevezésük');
  case 'field_medi':
    return _opr2fld('boolean_radio', 'Rendszeresen szedett gyógyszerek');
  case 'field_medi_detail2':
    return array (
      'type' => 'medical_dosage_field',
      'cardinality' => '5',
      'instance' => 
      array (
        'label' => 'részletezése',
        'widget' => 
        array (
          'type' => 'medical_dosage_widget',
        ),
        'settings' => 
        array (
          'display_how' => 1,
          'display_activesub' => '1',
          'display_notes' => '1',
          'how_options' => "intravenious\noral\nenteral",
          'unit_options' => "g\nmg",
          'dosage_type' => 'cctimes',
        ),
      ),
    );
  case 'field_diet':
    return _opr2fld('boolean_radio', 'Diéta');
  case 'field_diet_type':
    return _opr2fld('text', 'megnevezése');
  case 'field_history':
    return _opr2fld('textarea', 'Egyéb előzmény');
  case 'field_orsym_swal':
  case 'field_sym_swal':
    return _opr2fld('boolean_radio', 'Nyelési nehezítettség');
  case 'field_orsym_swal_since':
  case 'field_sym_swal_since':
    return _opr2fld('number_list', 'mióta', array(
      'options' => array(
        1 => "1 hét",
        2 => "2 hét",
        3 => "3 hét",
        4 => "4 hét",
        5 => "5 hét",
        6 => "6 hét",
        7 => "7 hét",
        8 => "8 hét",
        12 => "3 hónap",
        16 => "4 hónap",
        20 => "5 hónap",
        24 => "6 hónap",
        28 => "7 hónap",
        32 => "8 hónap",
        36 => "9 hónap",
        40 => "10 hónap",
        44 => "11 hónap",
        52 => "1 év",
        78 => "1,5 év",
        104 => "2 év",
        156 => "3 év",
        208 => "4 év",
        260 => "5 év",
        261 => "több, mint 5 év",
      ),
    ));
  case 'field_orsym_swal_food':
  case 'field_sym_swal_food':
    return _opr2fld('number_list', 'milyen étel mellett fordult elő', array(
      'options' => array(
        1 => 'szilárd',
        2 => 'pépes',
        3 => 'folyékony',
      ),
    ));
  case 'field_orsym_swal_freq':
  case 'field_orsym_chpr_freq':
  case 'field_orsym_regur_freq':
  case 'field_orsym_vomit_freq':
  case 'field_sym_swal_freq':
  case 'field_sym_chpr_freq':
  case 'field_sym_regur_freq':
  case 'field_sym_vomit_freq':
    return _opr2fld('number_list', 'milyen gyakran fordult elő', array(
      'options' => array(
        1 => 'minden étkezésnél',
        2 => 'naponta',
        3 => 'alkalomszerűen',
      ),
    ));
  case 'field_orsym_chpr':
  case 'field_sym_chpr':
    return _opr2fld('boolean_radio', 'Mellkasi nyomás');
  case 'field_orsym_regur':
  case 'field_sym_regur':
    return _opr2fld('boolean_radio', 'Ételregurgitáció');
  case 'field_orsym_vomit':
  case 'field_sym_vomit':
    return _opr2fld('boolean_radio', 'Hányás');
  case 'field_orsym_appet':
  case 'field_sym_appet':
    return _opr2fld('number_list', 'Étvágy', array(
      'options' => array(
        1 => 'jó',
        2 => 'megtartott',
        3 => 'rossz',
      ),
    ));
  case 'field_orsym_weight':
  case 'field_sym_weight':
    return _opr2fld('boolean_radio', 'Testsúly változás');
  case 'field_orsym_weight_sgn':
  case 'field_sym_weight_sgn':
    return _opr2fld('number_list', 'változás iránya', array(
      'options' => array(
        1 => 'fogyás',
        2 => 'hízás',
      ),
    ));
  case 'field_orsym_weight_time':
  case 'field_sym_weight_time':
    return _opr2fld('number_list', 'mennyi idő alatt', array(
      'options' => array(
        1 => '1 hét',
        2 => '2 hét',
        3 => '3 hét',
        4 => '1 hónap',
        8 => '2 hónap',
        12 => '3 hónap',
        16 => '4 hónap',
        20 => '5 hónap',
        24 => '6 hónap',
        52 => '6-12 hónap',
        53 => 'több, mint 1 év',

      ),
    ));
  case 'field_orsym_weight_amount':
  case 'field_sym_weight_amount':
    return _opr2fld('number_list', 'mennyit', array(
      'options' => array(
        1 => '1 kg',
        2 => '2 kg',
        3 => '3 kg',
        4 => '4 kg',
        5 => '5 kg',
        6 => '6 kg',
        7 => '7 kg',
        8 => '8 kg',
        9 => '9 kg',
        10 => '10 kg',
        11 => '11 kg',
        12 => '12 kg',
        13 => '13 kg',
        14 => '14 kg',
        15 => '15 kg',
        16 => '16 kg',
        17 => '17 kg',
        18 => '18 kg',
        19 => '19 kg',
        20 => '20 kg',
        21 => '20 kg-nál több',
      ),
    ));
  case 'field_orsym_cough':
  case 'field_sym_cough':
    return _opr2fld('boolean_radio', 'Köhögés');
  case 'field_orsym_thmed':
    return _opr2fld('boolean_radio', 'Gyógyszeres kezelés');
  case 'field_orsym_thbotox':
    return _opr2fld('boolean_radio', 'Botulinus toxin kezelés');
  case 'field_orsym_thbal':
    return _opr2fld('boolean_radio', 'Ballon dilatáció');
  case 'field_orsym_thhmy':
    return _opr2fld('boolean_radio', 'Heller myotomia');
  case 'field_orsym_thmed_swal':
  case 'field_orsym_thbotox_swal':
  case 'field_orsym_thbal_swal':
  case 'field_orsym_thhmy_swal':
    return _opr2fld('number_list', 'Nyelési nehezítettség', array(
      'options' => array(
        1 => 'jobb',
        2 => 'változatlan',
        3 => 'rosszabb',
      ),
    ));
  case 'field_orsym_thmed_chpr':
  case 'field_orsym_thbotox_chpr':
  case 'field_orsym_thbal_chpr':
  case 'field_orsym_thhmy_chpr':
    return _opr2fld('number_list', 'Mellkasi nyomás', array(
      'options' => array(
        1 => 'jobb',
        2 => 'változatlan',
        3 => 'rosszabb',
      ),
    ));
  case 'field_orsym_thmed_regur':
  case 'field_orsym_thbotox_regur':
  case 'field_orsym_thbal_regur':
  case 'field_orsym_thhmy_regur':
    return _opr2fld('number_list', 'Étel regurgitáció', array(
      'options' => array(
        1 => 'jobb',
        2 => 'változatlan',
        3 => 'rosszabb',
      ),
    ));
  case 'field_orsym_thmed_vomit':
  case 'field_orsym_thbotox_vomit':
  case 'field_orsym_thbal_vomit':
  case 'field_orsym_thhmy_vomit':
    return _opr2fld('number_list', 'Hányás', array(
      'options' => array(
        1 => 'jobb',
        2 => 'változatlan',
        3 => 'rosszabb',
      ),
    ));
  case 'field_orsym_thmed_appet':
  case 'field_orsym_thbotox_appet':
  case 'field_orsym_thbal_appet':
  case 'field_orsym_thhmy_appet':
    return _opr2fld('number_list', 'Étvágy', array(
      'options' => array(
        1 => 'jobb',
        2 => 'változatlan',
        3 => 'rosszabb',
      ),
    ));
  case 'field_orsym_thmed_weight':
  case 'field_orsym_thbotox_weight':
  case 'field_orsym_thbal_weight':
  case 'field_orsym_thhmy_weight':
    return _opr2fld('number_list', 'Testsúly-változás', array(
      'options' => array(
        1 => 'fogyás',
        2 => 'változatlan súly',
        3 => 'testsúly-növekedés',
      ),
    ));
  case 'field_orsym_thmed_cough':
  case 'field_orsym_thbotox_cough':
  case 'field_orsym_thbal_cough':
  case 'field_orsym_thhmy_cough':
    return _opr2fld('number_list', 'Köhögés', array(
      'options' => array(
        1 => 'jobb',
        2 => 'változatlan',
        3 => 'rosszabb',
      ),
    ));
  case 'field_orsym_thmed_esoburn':
  case 'field_orsym_thbotox_esoburn':
  case 'field_orsym_thbal_esoburn':
  case 'field_orsym_thhmy_esoburn':
    return _opr2fld('boolean_radio', 'Nyelőcső égés megjelent-e');
  case 'field_orsym_thmed_acid':
  case 'field_orsym_thbotox_acid':
  case 'field_orsym_thbal_acid':
  case 'field_orsym_thhmy_acid':
    return _opr2fld('boolean_radio', 'Savas regurgitáció megjelent-e');
  case 'field_orsym_thmed_epiburn':
  case 'field_orsym_thbotox_epiburn':
  case 'field_orsym_thbal_epiburn':
  case 'field_orsym_thhmy_epiburn':
    return _opr2fld('boolean_radio', 'Epigastrialis égő fájdalom megjelent-e');
  case 'field_orsym_swalrad':
    return _opr2fld('boolean_radio', 'Kontroll nyelési RTG eredmény');
  case 'field_orsym_esoman':
    return _opr2fld('boolean_radio', 'Kontroll nyelőcső manometria eredménye');
  case 'field_orsym_esoman_lesbp':
    return _opr2fld('number_decimal', 'LES alapnyomás', array(
      'suffix' => 'Hgmm',
    ));
  case 'field_orsym_esoman_lesrel':
    return _opr2fld('number_integer', 'LES relaxatio', array(
      'suffix' => '%',
    ));
  case 'field_sym_eckard':
    return _opr2fld('number_list', 'Felvételi Eckard score', array(
      'min' => 1,
      'max' => 12,
    ));

  case 'field_weight':
    return _opr2fld('number_decimal', 'Testsúly', array(
      'suffix' => 'kg',
    ));
  case 'field_height':
    return _opr2fld('number_decimal', 'Testmagasság', array(
      'suffix' => 'm',
    ));
  case 'field_bmi':
    return _opr2fld('number_decimal', 'BMI', array(
      'suffix' => 'kg/m2',
    ));
  case 'field_abdomtender':
    return _opr2fld('boolean_radio', 'Hasi nyomásérzékenység');

  case 'field_ther_med':
    return _opr2fld('boolean_radio', 'Gyógyszeres kezelés');
  case 'field_ther_med_type':
    return _opr2fld('number_list', 'gyógyszer típusa', array(
      'options' => array(
        1 => 'calcium csatorna blokkoló',
        2 => 'nitrát',
        3 => 'NO donor',
      ),
    ));
  case 'field_ther_med_dose':
    return _opr2fld('number_decimal', 'gyógyszer dózisa', array(
      'suffix' => 'mg',
    ));
  case 'field_ther_med_times':
    return _opr2fld('number_integer', 'naponta hányszor');
  case 'field_ther_botox':
    return _opr2fld('boolean_radio', 'Botulinus toxin kezelés');
  case 'field_ther_botox_dose':
    return _opr2fld('number_decimal', 'toxin dózisa');
  case 'field_ther_botox_times':
    return _opr2fld('number_integer', 'kezelés száma');
  case 'field_ther_baldil':
    return _opr2fld('boolean_radio', 'Ballon dilatáció');
  case 'field_ther_baldil_type':
    return _opr2fld('text', 'ballon típusa');
  case 'field_ther_baldil_len':
    return _opr2fld('number_decimal', 'tágítás hossza', array(
      'suffix' => 'mm',
    ));
  case 'field_ther_baldil_dil':
    return _opr2fld('number_decimal', 'tágítás mértéke', array(
      'suffix' => 'mm',
    ));
  case 'field_ther_baldil_rep':
    return _opr2fld('boolean_radio', 'Ismételt tágításra szükség volt-e');
  case 'field_ther_baldil_rep_times':
    return _opr2fld('number_integer', 'hányszor');
  case 'field_ther_baldil_comp':
  case 'field_ther_poem_comp':
  case 'field_ther_hemy_comp':
    return _opr2fld('boolean_radio', 'Szövődmény fellépett-e');
  case 'field_ther_baldil_comp_type':
    return _opr2fld('select_or_other', 'szövődmény típusa', array(
      'options' => array(
        1 => 'nyálkahártya sérülés',
        2 => 'perforáció',
        3 => 'láz',
      ),
    ));
  case 'field_ther_poem':
    return _opr2fld('boolean_radio', 'POEM');
  case 'field_ther_poem_comp_type':
    return _opr2fld('text', 'szövődmény típusa');
  case 'field_ther_poem_rep':
    return _opr2fld('boolean_radio', 'Ismételt beavatkozás volt-e');
  case 'field_ther_hemy':
    return _opr2fld('boolean_radio', 'Heller myotomia');
  case 'field_ther_hemy_type':
    return _opr2fld('text', 'Fundoplicatio típusa');
  case 'field_ther_hemy_comp_type':
    return _opr2fld('select_or_other', 'szövődmény típusa', array(
      'options' => array(
        1 => 'perforáció',
        2 => 'vérzés',
      ),
    ));
  case 'field_ther_hemy_reop':
    return _opr2fld('boolean_radio', 'Reoperációra szükséges volt-e');

  case 'field_epic':
    return _opr2fld('textarea', 'Epikrízis');
  }
}

